const express = require('express');
const passport = require('passport');
const router = express.Router();
const authVerify = require('../middleware/verifytoken');

const index = require('../services/index.service');
const auth = require('../services/auth.service');
const isLogin = (req,res,next) => {
    if(req.user){
        next();
    }else{
        res.status(401);
    }
}

router.get('/',index.getAll);
router.get('/task',authVerify,(req,res) => res.json({ data : "send data task"}));
router.post('/register', auth.register);
router.post('/singin', auth.singin);
router.get('/google', passport.authenticate('google', { scope: ['profile','email'] }));

router.get('/google/callback', passport.authenticate( 'google', {
        successRedirect: '/google/success',
        failureRedirect: '/google/failure'
}));
router.get('/google/success',isLogin,(req,res) => res.json({ data : "loged in success"}));
router.get('/google/failure', (req,res) => res.json({ data : "loged in failure"}));
router.get('/logout', (req,res) => {
    req.session = null;
    req.logout();
    res.redirect('/');
});

module.exports = router;