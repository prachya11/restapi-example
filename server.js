const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const cookieSession = require('cookie-session');
const router = require('./router/index');
const apiLimiter = require('./middleware/ratelimiter');
const app = express();
const port = 3000;
require('./middleware/passport');
require('dotenv').config();

app.use(cors());
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000')
  res.header('Access-Control-Allow-Methods','POST, GET, PUT, PATCH, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers','Content-Type, Option, Authorization')
  return next()
});
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}));


app.use('/',apiLimiter,router);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});