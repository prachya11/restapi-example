const db = require('../config/database');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const logger = require('../middleware/winstonlog');
const {registerValidator,singinValidator} = require('../validation/auth.validation');


const register = async (req, res) => {
    logger.info('auth register '+res.body.email,res.body.password);
    const { error } = registerValidator(req.body);
    if(error) res.status(400).json({
        message: error.details[0].message
    });

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    let user = {
        email: req.body.email,
        password: hashPassword,
        name: req.body.name
    }
    try {
        let target = await db('user').where('email', user.email);
        let getEmail = target.map(x => x.email);
        if(getEmail[0]) {
            throw new Error(
                logger.error('email already exists '+ res.body.email),
                res.json({
                    message: 'email already exists'
                })
            );
            
        }else{
            let target_id = await db('user').insert(user);
            if(target_id != null){
                logger.info('create success id '+ target_id),
                res.status(200).json({
                    message: 'success',
                    data: 'create user success user id is '+target_id
                });
            }
            else{

                logger.error('Bad Request'),
                res.status(400).json({
                    message: 'Bad Request',
                    data: ''
                });
            }
        }
        
    } catch (error) {
        console.log(error);
        logger.error('error : ', error);
    }
}

const singin = async (req, res) => {
    logger.info('auth singin '+ res.body.email,res.body.password);
    const { error } = singinValidator(req.body);
    if(error) res.status(400).json({
        message: error.details[0].message
    });

    let user = {
        email: req.body.email,
        password: req.body.password
    }
    try {

        let target = await db('user').where('email', user.email);
        let getpass = target.map(x => x.password);
        if(getpass[0] === undefined || getpass[0] === null){
            logger.error('not found user '+ res.body.email),
            res.status(400).json({
                message: 'not found user'
            });
        }
        else{
            
            let validPass = await bcrypt.compare(user.password,getpass[0]);
            if(!validPass){
                logger.error('Invalid Password '+ res.body.password),
                res.status(400).json({
                    message: 'Invalid Password'
                });
            }else{
  
                const token = jwt.sign({ id: target[0].id }, process.env.TOKEN_SECRET);
                logger.info('user-token '+ token);
                res.header('user-token',token).json({
                    message: 'success',
                    token: token
                });
            }
        }
        
        
    } catch (error) {
        console.log(error);
        logger.error('error : ', error);
    }
}

module.exports = {
    register,
    singin
}