const db = require('../config/database');
const logger = require('../middleware/winstonlog');

const getAll = async (req, res) => {
    logger.info('index getAll data');
    try {
        res.status(200).json({data:"you are not loged in"});
        logger.info('response success');
    } catch (error) {
        console.log(err)
        logger.error('error : ', error);
    }
}

module.exports = {getAll}