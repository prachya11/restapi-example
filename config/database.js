const knex = require('knex')({
    client: 'mysql2',
    connection: {
        host: process.env.HOST,
        user: process.env.USER,
        password: process.env.PASS_WORD,
        database: process.env.DATA_BASE
    }
});

module.exports = knex;