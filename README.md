#### framework ที่ทำงานเป็น api พร้อมบอกเหตุผลที่เลือก

- express js สาเหตุที่ใช้ framwork นี้ก็เพราะ ใช้งานง่ายเบา ติดตั้งได้ไว doc เยอะ มี community รองรับ หากใช้ framwork ตัวนี้ได้คล่องก็สามารถต่อยอดไปใช้ framwork ตัวอื่นได้เหมือนกัน เพราะการเขียนไม่ได้ต่าวกันมาก
- อีกอันที่อยากแนะนำคือ Nest.js framework ตัวนี้มีรูปแบการเขียนที่เป็น pattern ถ้าในทีมต้องการเขียนที่เป็นมาตรฐาน framwork ตัวนี้น่าจะตอบโจทย์ และยังใช้งานง่าย มี cli ให้ใช้ในตัว ติดตั้ง plugins เพิ่มแล้วยังสามรถ migrate กับ database ได้ด้วย


#### tool ที่นำมาใช้ทำ api document พร้อมบอกเหตุผลที่เลือก

- body-parser tool ที่ทำให้ใช้  parse application/json , parse application/x-www-form-urlencoded

- nodemon ตัวนี้จะใช้ทำ auto restart project ของเรานั้นเอง ไม่ต้องคอย npm start บ่อยๆ

- mysql หรือ sqlite3 ไว้ใช้ติดต่อ data base
####  ระบบ log file ที่คิดว่าเหมาะสม พร้อมบอกเหตุผลที่เลือก

- winston ใช้งานง่าย มีระดับ lavel ให้เลือกใช้งาน พร้อมทั้งยังสามารถเขียน function ให้เก็บลงไฟล์ได้ง่ายๆแล้วก็สามารถเพิ่ม timestamp เพื่อที่ไว้ใช้ดูเวลาในการเกิด log นั้น
#### framework ที่ทำ oauth2.0 พร้อมบอกเหตุผล

- jwt เป็นมาตรฐานตัวหนึ่งที่ใช้ การส่งข้อมูลแบบ json มาสร้างเป็นรหัส token และจใช้ . ในการคั้น เช่น aaa.bbb.ccc



#### framework ที่ทำ CRUD ต่อฐานข้อมูล 

- knexjs เป็น framwork ที่ใช้ดิดต่อกับ database ทางฝั่ง nodejs สามารถใช้ร่วมกับ database อื่นได้ database ที่รองรับ Postgres, MSSQL, MySQL, MariaDB, SQLite3, Oracle, Amazon Redshift และ data ยังมีแยก Header และ play load ไว้เก็บข้อมูลแต่ละส่วนกัน การติดตั้ง  npm install knex --save
  ```
  $ npm install pg
  $ npm install sqlite3
  $ npm install mysql
  $ npm install mysql2
  $ npm install oracledb
  $ npm install mssql
  ```

คำสั่งที่ใช้ select 
```javascript
knex.select().table('books')
```

#### framework ที่ไว้ทำ metric/rate-limit api

- express-rate-limit  มีไว้จำกัด limit request ที่ต้องการจำกัดไม่ให้เกิน เช่น 1 ชั่วโมงไม่เกิน 1000 request    ใช้แก้ปัญหา ddos attack

#### วิธีป้องกัน SQL Injection

- การป้องกัน sql injection ที่นิยมกันในปุัจจุบันนี้ เช่น การทำ Prepared Statements 

 ```javascript
  let sql = 'SELECT * FROM users WHERE id = '+ db.escape(req.params.id)
 ```

หรือ

```javascript
let userId = 5;
let query = connection.query('SELECT * FROM users WHERE id = ?', [userId], function(err, results) {
  //query.sql returns SELECT * FROM users WHERE id = '5'
});
```

  จากนั้น โปรแกรมก็จะแปลงเป็น string ทั้งหมด แล้วค่อยไปประมวลผล ก็สามารถป้องกันได้

- การใช้  Stored Procedure วิธีนี้จะเป็นาการเก็บข้อมูล query ไว้ที่ database แทน โปรแกรมจะคอยส่งเพียง parameter กับ data ให้ stored procedure
- อีกวิธีก็คือ ใช้พวก ORM เข้ามาใช้เชื่อมต่อ database แทน ก็ถือว่าสามารถป้องกันได้ เพราะ orm จะแปลงเป็น query ให้เรา

#### นำเอา component ข้างต้น มาเขียนเป็น code

https://gitlab.com/prachya11/restapi-example



#### นำ code push ขึ้น git hub  เพื่อตรวจสอบ code 

พอดี github มีปัญหา เรื่อง push code เลยต้องใช้ gitlab แทน

https://gitlab.com/prachya11/restapi-example



#### ก่อนทำงานให้ประเมินเวลาที่ทำ และหลังจากจบงานให้บอกระยะเวลาที่ใช้งานจริง

การประเมินเวลา การทำงานคาดว่าจะเสร็จภายใน เสาร์ อาทิตย์ 19 - 20 

หลังจากทำเสร็จจบงาน ใช้เวลาจริง 19, 20, 21 สามวัน 

19 หาข้อมูล วางโครง

20 dev

21 test and เก็บ log
