const passport = require('passport');
const GoogleStrategy = require( 'passport-google-oauth2' ).Strategy;

passport.serializeUser(function(user, done) {
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new GoogleStrategy({
    clientID: '927044002747-7667dv6dupr34unkt4v8fqn2rio1aivl.apps.googleusercontent.com',
    clientSecret: 'SH6sOXPRTTQidFl7PGaZq95G',
    callbackURL: 'http://localhost:3000/',
    passReqToCallback   : true
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
        return done(err, user.id);
      });
  }
));