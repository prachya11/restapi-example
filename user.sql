/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50525
 Source Host           : 127.0.0.1:3306
 Source Schema         : restapi-example

 Target Server Type    : MySQL
 Target Server Version : 50525
 File Encoding         : 65001

 Date: 21/12/2020 01:38:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET tis620 COLLATE tis620_thai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = tis620 COLLATE = tis620_thai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (4, 'prachya2@gmail.com', '$2a$10$/bVW609yjP4I8S0U9UbXsuXzJr8Ak/Y6845erpPLd1oqOBdG2GT2i', 'tomy2');
INSERT INTO `user` VALUES (6, 'prachya3@gmail.com', '$2a$10$y4fyDNfBMUtNeU0GwtZrc.6FOBWoCc89f/ApPm/DbTcBGqS6kFxi2', 'tomy3');
INSERT INTO `user` VALUES (7, 'prachya4@gmail.com', '$2a$10$X6WWQy2ZlZTcfcKX4f/O8u3mjYy3av.oTsU62VEklfgfG6.7qVtOK', 'tomy4');

SET FOREIGN_KEY_CHECKS = 1;
